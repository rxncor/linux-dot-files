
# Linux Dot Files for Chris Barnett

Following in the footsteps of Patrick Wyatt (git@github.com:webcoyote/dotfiles.git) I am placing my dot files under version control.

Links dotfiles from this repository into their correct locations in $HOME
Most important are .bash*, .git* , .vimrc and .vmdrc

# Before you start

The dot files are linked using a force statement which overwrites existing files. Please backup your local files first.

# Installation

git clone git@bitbucket.org:rxncor/linux-dot-files.git  ~/.linux-dot-files

~/.linux-dot-files/configure.sh 


# Common Issues 
## vim related 
Note that the .vimrc depends on some plugins. If these are not installed on your system then:
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim +PlugInstall +qall

## what is being sourced in bashrc
local config and modules for the unit which is stored on the shared drive and git@git.cem.uct.ac.za:lab_information/modules-and-dotfiles.git
