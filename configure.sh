#!/bin/bash
#  Patrick Wyatt's code
# Link all configuration files in the ./home directory and its children into ~
#

# Set the directory to the location of this script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $SCRIPT_DIR

# Some computers have "always_set_home" configured in the
# /etc/sudoers file for security. Instead of assuming that
# environment variables are preserved, manually set the
# USER and HOME variables. This ensures that the script
# will run properly when executed by Chef under CentOS.
if [ "$USER" == "root" ]; then
  USER=`whoami`
  HOME="/home/$USER"
fi

if [ $(id -u) -eq 0 ]
then
  echo 'ERROR: This script should not be run as sudo or root.'
  exit
fi

# Link all configuration files in the ./home directory into ~
function recursively_link_files () {
  for file in $1/?*; do
    if [[ -d $file ]]; then
      mkdir -p $2/`basename $file`
      recursively_link_files $file $2/`basename $file`
    else
      echo linking $file '=>' $2/`basename $file`
      ln -s -f $file $2/`basename $file`
    fi
  done
}
shopt -s dotglob
recursively_link_files $SCRIPT_DIR/home $HOME
shopt -u dotglob

