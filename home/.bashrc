# DO NOT MAKE CHANGES TO THIS FILE 
# ANY CHANGES MAY BE REMOVED
# - RATHER MAKE CHANGES TO .bashrc-personal
# source local bashrc
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# source SCRU bashrc
if [ -f /home/people/profile/bash/.bashrc-SCRU-general ]; then
        source /home/people/profile/bash/.bashrc-SCRU-general
fi

# source personal
if [ -f $HOME/.bashrc-personal ]; then
        source $HOME/.bashrc-personal > /dev/null 2>&1
fi

